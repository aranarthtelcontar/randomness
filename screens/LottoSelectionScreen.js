import React, { useState } from "react";
import {
  FlatList,
  StyleSheet,
  View,
} from "react-native";

import TitleText from "../components/TitleText";
import Card from "../components/Card";
import ControlButton from "../components/ControlButton";

/**
 * Returns the lotto selection screen which is the app for predefined lotteries.
 * @param {*} props lSelectVisibleSwitcher={lSelectVisibleSwitcher}
                    setType={setType}
                    makeDraw={makeDraw}
                    lottery={lottery}
                    drawList={drawList}
                    clearListHandler={clearListHandler}
                    renderSpecial={renderSpecial}
                    renderListItem={renderListItem}
 */
const LottoSelectionScreen = (props) => {
  // console.log("LottoSelectionScreen called");
  // console.log(props.drawList);
  return (
    <View style={styles.screen}>
      <View style={styles.main}>
        <View style={styles.title}>
          <TitleText>Lottery: {props.lottery.name}</TitleText>
        </View>
        <Card style={styles.resultCard}>
          <View style={styles.resultTitle}>
            <TitleText></TitleText>
          </View>
          <View style={styles.listContainer}>
            <FlatList
              conentContainerStyle={styles.flatList}
              data={props.drawList}
              getItemLayout={(data, index) => ({
                length: 33.7,
                offset: 33.7 * index,
                index,
              })}
              renderItem={(itemData) => props.renderListItem(itemData)}
            />
          </View>
        </Card>
      </View>
      <View style={styles.controlButtonArea}>
        <View style={styles.lottoControlArea}>
          <View style={{ ...styles.controlButton, marginLeft: 0 }}>
            <ControlButton
              onPress={() => props.makeDraw(props.lottery.settings)}
            >
              Cast
            </ControlButton>
          </View>
          <View style={styles.controlButton}>
            <ControlButton onPress={() => props.clearListHandler()}>
              Clear
            </ControlButton>
          </View>
        </View>
        <View style={styles.navigationContainer}>
          <ControlButton
            onPress={() => {
              props.setType("");
              props.lSelectVisibleSwitcher();
              props.clearListHandler();
            }}
          >
            Back
          </ControlButton>
        </View>
      </View>
    </View>
  );
};

export default LottoSelectionScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
  },
  main: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  title: {
    marginBottom: 10,
  },
  resultCard: {
    flex: 1,
    marginBottom: 10,
    width: "90%",
  },
  resultTitle: {
    alignItems: "center",
  },

  listContainer: {
    flex: 1,
  },
  flatList: {
    flexGrow: 1,
  },

  controlButtonArea: {
    width: "100%",
  },
  lottoControlArea: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  controlButton: {
    flex: 1,
    marginLeft: 10,
  },
  navigationContainer: {},
});
