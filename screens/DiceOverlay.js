import React, { useState } from "react";
import {
  Modal,
  Picker,
  StyleSheet,
  View,
  TouchableHighlight,
} from "react-native";

import BodyText from "../components/BodyText";
import ControlButton from "../components/ControlButton";
import colors from "../constants/colors";

/**
 * Returns the modal overlay for setting dice sides or dice modifiers.
 * 
 * @param {} props  overlayTitle={overlayTitle}
                    setOverlayTitle={setOverlayTitle}
                    overlayIsVisible={overlayIsVisible}
                    overlayVisibilitySwitcher={overlayVisibilitySwitcher}
                    diceToUpdate={diceToUpdate}
                    setDiceToUpdate={setDiceToUpdate}
                    diceListUpdateHandler={diceListUpdateHandler}
                    pickerItems={pickerItems}
                    setPickerItems={setPickerItems}
 */
const DiceOverlay = (props) => {
  const cancelButton = () => {
    console.log("DiceOverlay > cancelButton called");
    props.setOverlayTitle("");
    props.setDiceToUpdate({});
    props.setPickerItems([]);
    props.overlayVisibilitySwitcher();
    console.log("DiceOverlay: overlayVisibilitySwitcher called");
  };

    if (props.overlayIsVisible) {
      console.log("DiceOverlay called");
      console.log(
        "DiceOverlay >" +
          "\n  props.dice.key: " +
          props.diceToUpdate.key +
          "\n  props.dice.sides: " +
          props.diceToUpdate.value +
          "\n  props.diceToUpdate.value: " +
          props.diceToUpdate.value
      );
      
      // Return picker items
      const pickerItemList = () => {
        return props.pickerItems.map((itemValue, itemKey) => {
          return <Picker.Item label={itemValue} key={itemKey} value={itemValue} />;
        });
      };

      return (
        <Modal
          animation="slide"
          transparent={true}
          visible={props.overlayIsVisible}
          onRequestClose={() => {
            Alert.alert("Exiting settings.");
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <BodyText style={{ ...styles.modalText, marginBottom: 20 }}>
                {props.overlayTitle}
              </BodyText>

              <View>
                <View style={styles.pickerContainer}>
                  <Picker
                    selectedValue={props.diceToUpdate.value.toString()}
                    style={{ height: 50, width: 200 }}
                    onValueChange={(itemValue, itemIndex) =>
                      props.diceListUpdateHandler(
                        props.diceToUpdate.key,
                        itemValue
                      )
                    }
                  >
                    {pickerItemList()}
                    {/* {props.pickerItems.map((item) => {
                      return <Picker.Item label={item} value={item} />;
                    })} */}
                  </Picker>
                </View>
              </View>

              <ControlButton
                style={styles.modalButton}
                onPress={() => cancelButton()}
              >
                Done
              </ControlButton>
            </View>
          </View>
        </Modal>
      );
    } else {
      return null;
    }
};

export default DiceOverlay;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "60%",
    height: "60%",
    margin: 20,
    backgroundColor: colors.accent,
    borderRadius: 10,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalText: {
    fontSize: 17,
    textAlign: "center",
  },
  modalButton: {
    backgroundColor: colors.background,
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  pickerContainer: {
    height: 200,
    paddingTop: 40,
    alignItems: "center",
  },
});
