import React, { useState } from "react";
import {
  Modal,
  Picker,
  StyleSheet,
  View,
  Dimensions,
} from "react-native";

import BodyText from "../components/BodyText";
import ControlButton from "../components/ControlButton";
import colors from "../constants/colors";

import data from "../constants/data";

const pickerItemList = () => {
  return data.lotterySelection.map((item) => {
    return <Picker.Item label={item.name} key={item.key} value={item.name} />;
  });
};

const getLottery = (lotteryName) => {
  return data.lotterySelection.filter(
    (listItem) => listItem.name === lotteryName
  )[0];
};

/**
 * Returns a modal overlay screen on which the user picks a Lottery from a
 * predefined list.
 * 
 * @param {*} props listVisible={props.listVisible}
                    listVisibleSwitcher={props.listVisibleSwitcher}
                    lSelectVisibleSwitcher={props.lSelectVisibleSwitcher}
                    lottery={props.lottery}
                    setLotteryHandler={props.setLotteryHandler}
                    setType={props.setType}
 */
const LottoListOverlay = (props) => {
  // console.log("LottoListOverlay");

  if (false) {
    return null;
  } else {
    let pickerResponsive = styles.pickeriOS; // ToDo: Add iOS/Android check
    return (
      <Modal
        animation="slide"
        transparent={true}
        visible={props.listVisible}
        onRequestClose={() => {
          Alert.alert("Exiting settings.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <BodyText style={styles.modalText}>Lotto Selection List</BodyText>

              <View style={styles.pickerContainerAndroid}>
                <Picker
                  selectedValue={props.lottery.name}
                  style={pickerResponsive}
                  onValueChange={(lotteryName) =>
                    props.setLotteryHandler(getLottery(lotteryName))
                  }
                >
                  {pickerItemList()}
                </Picker>
              </View>

            <View style={styles.overlayControlContainer}>
              <View style={styles.buttonContainer}>
                <ControlButton
                  onPress={() => {
                    props.listVisibleSwitcher();
                    props.setType("");
                  }}
                >
                  Back
                </ControlButton>
              </View>
              <View style={styles.buttonContainer}>
                <ControlButton
                  onPress={() => {
                    props.setType("S");
                    props.lSelectVisibleSwitcher();
                    props.listVisibleSwitcher();
                  }}
                >
                  Start
                </ControlButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
};

export default LottoListOverlay;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "70%",
    height: "60%",
    backgroundColor: colors.accent,
    borderRadius: 10,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 1,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalText: {
    fontSize: Dimensions.get("window").width < 350 ? 17 : 20,
    textAlign: "center",
  },
  modalButton: {
    backgroundColor: colors.background,
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  pickerContainerAndroid: {
    height: Dimensions.get("window").height * 0.33,
    paddingTop: 5,
    alignItems: "center",
  },
  pickeriOS: {
    height: 50,
    width: 200,
  },
  overlayControlContainer: {
    flexDirection: "row",
  },
  buttonContainer: {
    marginHorizontal: 5,
  },
});
