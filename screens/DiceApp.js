import React, { useState, useEffect } from "react";
import {
  Dimensions,
  FlatList,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from "react-native";

import BodyText from "../components/BodyText";
import TitleText from "../components/TitleText";
import Card from "../components/Card";
import DiceOverlay from "./DiceOverlay";
import AppSelectButton from "../components/AppSelectButton";
import ControlButtonSmall from "../components/ControlButtonSmall";
import RemoveItemButton from "../components/RemoveItemButton";
import colors from "../constants/colors";
import data from "../constants/data";

/**
 * Returns the dice app screen.
 * 
 * @param {*} props receives onBack={startAppHandler} to return to selection
 *                  screen.
 */
const DiceApp = (props) => {
  const [diceList, setDiceList] = useState([
    {
      key: new Date().getTime().toString(),
      sides: 6,
      modifier: 0,
      result: "",
    },
  ]);
  const [overlayIsVisible, setOverlayIsVisible] = useState(false);
  const [overlayTitle, setOverlayTitle] = useState("");
  const [diceToUpdate, setDiceToUpdate] = useState({});
  const [pickerItems, setPickerItems] = useState([]);
  const [totalResult, setTotalResult] = useState("");
  const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
    Dimensions.get("window").width
  );
  const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
    Dimensions.get("window").height
  );
  
  /**
   * Get display dimensions.
   */
  useEffect(() => {
    const updateLayout = () => {
      setAvailableDeviceHeight(Dimensions.get("window").height);
      setAvailableDeviceWidth(Dimensions.get("window").width);
    };

    Dimensions.addEventListener("change", updateLayout);

    return () => {
      Dimensions.removeEventListener("change", updateLayout);
    };
  });

  /**
   * Switches the visibility of the modal.
   */
  const overlayVisibilitySwitcher = () => {
    setOverlayIsVisible(!overlayIsVisible);
  };

  /**
   * Removes a dice from the diceList useState by the given key.
   * 
   * @param {} removeKey the key
   */
  const removeDiceByKey = (removeKey) => {
    const newList = diceList.filter((item) => item.key !== removeKey);
    setDiceList(newList);
    getTotal(newList);
  };

  /**
   * Adds a default dice.
   */
  const addDice = () => {
    setDiceList((curDiceList) => [
      ...curDiceList,
      {
        key: new Date().getTime().toString(),
        sides: 6,
        modifier: 0,
        result: "",
      },
    ]);
  };

  /**
   * Resets all values.
   */
  const resetInputHandler = () => {
    setDiceList([]);
    setTotalResult(null);
    addDice();
  };

  /**
   * Calls the modal overlay to set dice sides.
   * 
   * @param {*} dice the dice object
   */
  const setDiceSideOverlayHandler = (dice) => {
    // console.log("setDiceSideOverlayHandler called");
    setDiceToUpdate({ key: dice.key, value: dice.sides, type: "S" });
    setPickerItems(data.diceSides);
    setOverlayTitle("Set dice sides");
    overlayVisibilitySwitcher();
  };

  /**
   * Calls the modal overlay to set the modifier for a dice.
   * 
   * @param {*} dice the dice object
   */
  const setDiceModifierOverlayHandler = (dice) => {
    // console.log("setDiceModifierOverlayHandler called");
    setDiceToUpdate({ key: dice.key, value: dice.modifier, type: "M" });
    setPickerItems(data.diceModifiers);
    setOverlayTitle("Set modifier");
    overlayVisibilitySwitcher();
  };

  /**
   * Updates an existing dice. Will read diceToUpdate useState and set the dice
   * object in the list with given itemKey to the given value.
   * 
   * @param {*} itemKey   the key of the dice
   * @param {*} itemValue the value to set
   */
  const diceListUpdateHandler = (itemKey, itemValue) => {
    // console.log("DiceApp > diceListUpdateHandler called");
    // console.log("  key: " + itemKey + "\n  new dice sides: " + itemValue);
    const index = diceList.findIndex((dice) => dice.key === itemKey);
    if (diceToUpdate.type === "S") { // Setting sides
      diceList[index].sides = itemValue;
      setDiceList([...diceList]);
      setDiceToUpdate({ key: itemKey, value: itemValue });
    } else if (diceToUpdate.type === "M") { // Setting modifiers
      diceList[index].modifier = itemValue;
      setDiceList([...diceList]);
    }
    setDiceToUpdate({
      key: diceToUpdate.key,
      value: itemValue,
      type: diceToUpdate.type,
    });
    overlayVisibilitySwitcher();
    return null;
  };

  /**
   * Returns a number as String with leading zeros
   *  
   * @param {*} number  a number as String or numerical
   * @param {*} width   the total amount of number places
   */
  const zeroFill = (number, width) => {
    // console.log("zeroFill called");
    let isNegative = false;
    let s = number + "";
    // console.log("number: " + s + " isNegative: "+ isNegative + ".");
    if (s.charAt(0) === "-") {
      isNegative = true;
      s = s.replace("-", "");
    }
    while (s.length < width) s = "0" + s;
    if (isNegative) {
      s = "-" + s;
    }
    return s;
  };

  /**
   * Testing
   */
/*  const testGenerateRN = () => {
    let min = 6;
    let max = 1;
    let sum = 0;
    const minMax = (item) => {
      sum += item;
      if (item < min) {
        min = item;
      }
      if (item > max) {
        max = item;
      }
    };
    let results = [];
    var i = 0;
    for (; i < 100; i++) {
      results.push(parseInt(generateRN(1, 6, -2), 10));
    }
    results.forEach(minMax);
    console.log("sum: " + sum);

    const average = sum / results.length;
    console.log("average: " + average);

    console.log("min: " + min + ", max: " + max);
    return results.length;
  }; */

  /**
   * Generates a random number/casts the dice and return the result with leading
   * zeros.
   * 
   * @param {*} minimum lowest number is always 1
   * @param {*} maximum maximum number/dice sides
   * @param {*} mod     modificator adds to result
   */
  const generateRN = (minimum, maximum, mod) => {
    // console.log("generateRN called");
    const min = parseInt(minimum, 10);
    const max = parseInt(maximum, 10);
    // console.log("  min: " + min + "\n  max: " + max);
    let numLength;
    const minLen = min.toString().replace("-", "").length;
    const maxLen = max.toString().replace("-", "").length;
    if (minLen < maxLen) {
      numLength = maxLen;
    } else {
      numLength = minLen;
    }
    const randomNumber =
      Math.floor(Math.random() * Math.floor(max + 1 - min)) +
      Math.floor(min) +
      parseInt(mod, 10);
    // console.log("  randomNumber/dice cast: " + randomNumber);
    return zeroFill(randomNumber, numLength);
  };

  /**
   * Returns a dice object with a random number result.
   * 
   * @param {*} dice  the dice object for which to generate a random number/
   *                  cast.
   */
  const generateRandomDice = (dice) => {
    // console.log("generateRandomDice called");
    const cast = generateRN(1, dice.sides, dice.modifier);
    // console.log("cast: " + cast);
    // Return a copy of the dice object with the current result.
    return {
      key: dice.key,
      sides: dice.sides,
      modifier: dice.modifier,
      result: cast,
    };
  };

  /**
   * Casts all dice in the diceList useState.
   */
  const castDice = () => {
    const newList = diceList.map((item) => generateRandomDice(item));
    setDiceList(newList);
    getTotal(newList);
  };

  /**
   * Calculates the total of dice casts and sets the totalResult useState.
   * 
   * @param {*} list the list with all dice objects
   */
  const getTotal = (list) => {
    if (list.length > 1) {
      let total = 0;
      list.forEach((item) => (total += parseInt(item.result, 10)));
      setTotalResult(total);
    }
  };

  const renderListItem = (itemData) => {
    let firstLine;
    if (itemData.index === 0) {
      firstLine = styles.noMarginTop;
    }
    return (
      <View style={[styles.listItem, firstLine]}>
        <View style={styles.diceListCol1}>
          <RemoveItemButton
            style={styles.listText}
            onPress={() => setDiceSideOverlayHandler(itemData.item)}
            name={itemData.item.key}
          >
            {itemData.item.sides}
          </RemoveItemButton>
        </View>
        <View style={styles.diceListCol2}>
          {/* <BodyText style={styles.listText}>{itemData.item.modifier}</BodyText> */}
          <RemoveItemButton
            style={styles.listText}
            onPress={() => setDiceModifierOverlayHandler(itemData.item)}
            name={itemData.item.key}
          >
            {itemData.item.modifier}
          </RemoveItemButton>
        </View>
        <View style={styles.diceListCol3}>
          <BodyText style={styles.listText}>{itemData.item.result}</BodyText>
        </View>
        <View style={styles.removeButtonContainer}>
          <RemoveItemButton
            style={styles.removeDice}
            onPress={() => removeDiceByKey(itemData.item.key)}
            name={itemData.item.key}
          >
            x
          </RemoveItemButton>
        </View>
      </View>
    );
  };

  let buttonStyle = styles.buttonMedium;

  if (Dimensions.get("window").width < 350) {
    console.log("dimension smaller than 350");
    buttonStyle = styles.buttonSmall;
  } else {
    console.log("normal dimension");
  }
  return (
    <View style={styles.screen}>
      <DiceOverlay
        overlayTitle={overlayTitle}
        setOverlayTitle={setOverlayTitle}
        overlayIsVisible={overlayIsVisible}
        overlayVisibilitySwitcher={overlayVisibilitySwitcher}
        diceToUpdate={diceToUpdate}
        setDiceToUpdate={setDiceToUpdate}
        diceListUpdateHandler={diceListUpdateHandler}
        pickerItems={pickerItems}
        setPickerItems={setPickerItems}
      />

      <View style={styles.titleContainer}>
        <TitleText style={styles.title}>Dice</TitleText>
      </View>
      <Card style={styles.cardContainer}>
        <View style={styles.diceListHeading}>
          <View style={styles.diceListCol1}>
            <BodyText style={styles.diceListHeadingText}>Sides</BodyText>
          </View>
          <View style={styles.diceListCol2}>
            <BodyText style={styles.diceListHeadingText}>Mod.</BodyText>
          </View>
          <View style={styles.diceListCol3}>
            <BodyText style={styles.diceListHeadingText}>Result</BodyText>
          </View>
        </View>
        <View style={[styles.listContainer, styles.marginTop]}>
          <FlatList
            data={diceList}
            renderItem={renderListItem}
            contentContainerStyle={styles.list}
          ></FlatList>
        </View>
        <View style={[styles.diceListHeading, styles.marginTop]}>
          <View style={styles.diceListCol1}>
            <BodyText style={styles.diceListHeadingText}></BodyText>
          </View>
          <View style={styles.diceListCol2}>
            <BodyText style={{...styles.diceListHeadingText, }}>Total:</BodyText>
          </View>
          <View style={styles.diceListCol3}>
            <BodyText style={styles.diceListHeadingText}>
              {totalResult}
            </BodyText>
          </View>
        </View>
        {/* <View style={styles.resultContainer}>
          <View style={styles.resultField}>
            <BodyText>Total:</BodyText>
          </View>
        </View> */}
      </Card>
      <TouchableWithoutFeedback style={styles.buttonContainerWrapper}>
        <View style={styles.buttonContainer}>
          <AppSelectButton
            style={buttonStyle}
            onPress={() => props.onBack("AppSelectionScreen")}
          >
            Back
          </AppSelectButton>
          <ControlButtonSmall style={buttonStyle} onPress={addDice}>
            Add
          </ControlButtonSmall>
          <ControlButtonSmall style={buttonStyle} onPress={castDice}>
            Cast Dice
          </ControlButtonSmall>
          <ControlButtonSmall style={buttonStyle} onPress={resetInputHandler}>
            Clear
          </ControlButtonSmall>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default DiceApp;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    paddingBottom: 0,
    alignItems: "center",
  },

  titleContainer: {
    width: "80%",
  },
  title: {
    fontSize: 20,
    marginBottom: 10,
    fontFamily: "open-sans-bold",
    textAlign: "center",
  },

  cardContainer: {
    flexGrow: 1,
    marginBottom: 15,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    width: "90%",
  },
  diceListHeading: {
    flexDirection: "row",
  },
  diceListCol1: {
    width: 80,
    padding: 5,
  },
  diceListCol2: {
    width: 80,
    padding: 5,
  },
  diceListCol3: {
    width: 80,
    padding: 5,
  },
  removeButtonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  removeDice: {
    padding: 5,
  },
  diceListHeadingText: {
    textAlign: "center",
  },
  listCardContainer: {
    backgroundColor: "black",
    width: "90%",
  },
  listContainer: {
    flex: 1,
    flexGrow: 1,
  },
  list: {
    width: "100%",
  },
  indexContainer: {
    justifyContent: "center",
    marginRight: 10,
    width: 25,
    borderRadius: 10,
    paddingLeft: 3,
  },
  listText: {
    fontSize: 18,
    textAlign: "center",
  },
  listItem: {
    flexDirection: "row",
    marginTop: 1,
    backgroundColor: colors.background,
  },

  resultContainer: {
    width: "90%",
    backgroundColor: "grey",
  },

  buttonContainerWrapper: {
    flex: 1,
  },
  buttonContainer: {
    height: 50,
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    marginTop: 15,
    alignItems: "flex-end",
  },
  buttonMedium: {
    paddingHorizontal: 15,
    marginBottom: 5,
  },
  buttonSmall: {
    marginVertical: 10,
    paddingHorizontal: 10,
  },

  marginTop: {
    marginTop: 2,
  },
  noMarginTop: { marginTop: 0 },
});
