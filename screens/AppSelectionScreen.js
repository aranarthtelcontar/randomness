import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
} from "react-native";

import BodyText from "../components/BodyText";
import TitleText from "../components/TitleText";
import Card from "../components/Card";
import AppSelectButton from "../components/AppSelectButton";

/**
 * Returns the initial app selection screen.
 * 
 * @param {*} props receives onBack={startAppHandler} or onStartApp={
 *                  startAppHandler} from App class.
 */
const AppSelectionScreen = props => {
  return (
    <TouchableWithoutFeedback>
      <View style={styles.screen}>
        <View style={styles.titleContainer}>
          <TitleText style={styles.title}>
            What randomness are you looking for?
          </TitleText>
        </View>
        <Card style={styles.selectionCard}>
          <BodyText style={styles.selectionTitle}>Select an App</BodyText>
          <View style={styles.buttonContainer}>
            <View style={styles.button}>
              <AppSelectButton onPress={() => props.onStartApp("NumberApp")}>
                Number
              </AppSelectButton>
              {/* <Button title="Number" onPress={() => props.onStartApp("NumberApp")} /> */}
            </View>
            <View style={styles.button}>
              <AppSelectButton onPress={() => props.onStartApp("DiceApp")}>
                Dice
              </AppSelectButton>
            </View>
            <View style={styles.button}>
              <AppSelectButton
                onPress={() => props.onStartApp("LotteryApp")}
              >
                Lottery
              </AppSelectButton>
            </View>
          </View>
        </Card>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AppSelectionScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },
  titleContainer: {
    width: "80%",
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
    fontFamily: "open-sans-bold",
    textAlign: "center",
  },
  selectionCard: {
    width: "80%",
    alignItems: "center",
  },
  selectionTitle: {
    marginBottom: 10,
  },
  buttonContainer: {
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 15,
    alignItems: "center",
  },
  button: { width: "80%", marginVertical: 10 },
});
