import React, { useState } from "react";
import { Alert, FlatList, StyleSheet, View } from "react-native";

import BodyText from "../components/BodyText";
import TitleText from "../components/TitleText";
import Card from "../components/Card";
import ControlButton from "../components/ControlButton";
import colors from "../constants/colors";

import Input from "../components/Input";

const LottoCustomScreen = (props) => {
  // console.log("\n\nLottoCustomScreen called");
  // console.log(props.drawList);
  const setNumberAmountOnEditHandler = () => {
    if (parseInt(props.numberAmount, 10) >= parseInt(props.numberField, 10)) {
      Alert.alert(
        "Error",
        "The amount of numbers to select has to be less than the amount of numbers to choose from",
        [
          {
            text: "OK",
            onPress: () =>
              props.setNumberAmount(
                (parseInt(props.numberField, 10) - 1).toString()
              ),
          },
        ],
        { cancelable: false }
      );
    }
  };

  const setNumberFieldOnEditHandler = () => {
    if (parseInt(props.numberField, 10) <= props.numberAmount) {
      Alert.alert(
        "Error",
        "The field of numbers to choose from has to be bigger than the amount of numbers to select",
        [
          {
            text: "OK",
            onPress: () =>
              props.setNumberField(
                (parseInt(props.numberAmount, 10) + 1).toString()
              ),
          },
        ],
        { cancelable: false }
      );
    }
  };
  const setSpecialAmountOnEditHandler = () => {
    if (parseInt(props.specialAmount, 10) >= props.specialField) {
      Alert.alert(
        "Error",
        "The amount of additional numbers to select has to be less than the amount of additional numbers to choose from",
        [
          {
            text: "OK",
            onPress: () =>
              props.setSpecialAmount(
                (parseInt(props.specialField, 10) - 1).toString()
              ),
          },
        ],
        { cancelable: false }
      );
    }
  };
  const setSpecialFieldOnEditHandler = () => {
    if (parseInt(props.specialField, 10) <= props.specialAmount) {
      Alert.alert(
        "Error",
        "The field of special numbers to choose from has to be bigger than the amount of special numbers to select",
        [
          {
            text: "OK",
            onPress: () =>
              props.setSpecialField(
                (parseInt(props.specialAmount, 10) + 1).toString()
              ),
          },
        ],
        { cancelable: false }
      );
    }
  };

  return (
    <View style={styles.screen}>
      <View style={styles.main}>
        <View style={styles.title}>
          <TitleText>Custom Lottery</TitleText>
        </View>
        <Card style={styles.settingsCard}>
          <View style={{ ...styles.settingsItem, marginBottom: 10 }}>
            <View style={styles.settingsItemHeader}>
              <TitleText style={styles.settingsText}>Main Numbers</TitleText>
            </View>
            <View style={styles.settingsInputContainer}>
              <Input
                style={styles.input}
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={2}
                onChangeText={(input) => {
                  props.setNumberAmount(input.toString());
                }}
                onEndEditing={() => setNumberAmountOnEditHandler()}
                value={props.numberAmount}
              ></Input>
              <View style={{ marginLeft: 30, marginRight: 30 }}>
                <BodyText>of</BodyText>
              </View>
              <View style={{ marginRight: 10 }}>
                <BodyText>1 to</BodyText>
              </View>
              <Input
                style={styles.input}
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={2}
                onChangeText={(input) => {
                  props.setNumberField(input.toString());
                }}
                onEndEditing={() => setNumberFieldOnEditHandler()}
                value={props.numberField}
              ></Input>
            </View>
          </View>
          <View style={styles.settingsItem}>
            <View style={styles.settingsItemHeader}>
              <TitleText style={styles.settingsText}>
                Additional Numbers
              </TitleText>
            </View>
            <View style={styles.settingsInputContainer}>
              <Input
                style={styles.input}
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={2}
                onChangeText={(input) => {
                  props.setSpecialAmount(input.toString());
                }}
                onEndEditing={() => setSpecialAmountOnEditHandler()}
                value={props.specialAmount}
              ></Input>
              <View style={{ marginLeft: 30, marginRight: 30 }}>
                <BodyText>of</BodyText>
              </View>
              <View style={{ marginRight: 10 }}>
                <BodyText style={{fontSize: 15,}}>1 to</BodyText>
              </View>
              <Input
                style={styles.input}
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={2}
                onChangeText={(input) => {
                  props.setSpecialField(input.toString());
                }}
                onEndEditing={() => setSpecialFieldOnEditHandler()}
                value={props.specialField}
              ></Input>
            </View>
          </View>
        </Card>
        <Card style={styles.resultCard}>
          <View style={styles.resultTitle}>
            <TitleText></TitleText>
          </View>
          <View style={styles.listContainer}>
            <FlatList
              conentContainerStyle={styles.flatList}
              data={props.drawList}
              getItemLayout={(data, index) => ({
                length: 33.7,
                offset: 33.7 * index,
                index,
              })}
              renderItem={(itemData) => props.renderListItem(itemData)}
            />
          </View>
        </Card>
      </View>
      <View style={styles.controlButtonArea}>
        <View style={styles.lottoControlArea}>
          <View style={{ ...styles.controlButton, marginLeft: 0 }}>
            <ControlButton
              onPress={() =>
                props.makeDraw([
                  props.numberAmount,
                  props.numberField,
                  props.specialAmount,
                  props.specialField,
                ])
              }
            >
              Cast
            </ControlButton>
          </View>
          <View style={styles.controlButton}>
            <ControlButton onPress={() => props.clearCustomLottoHandler("C")}>
              Clear
            </ControlButton>
          </View>
        </View>
        <View style={styles.navigationContainer}>
          <ControlButton
            onPress={() => {
              props.setType("");
              props.lSelectVisibleSwitcher();
              props.clearListHandler();
              props.clearCustomLottoHandler("S");
            }}
          >
            Back
          </ControlButton>
        </View>
      </View>
    </View>
  );
};

export default LottoCustomScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
  },
  main: {
    flex: 1,
    width: "100%",
    alignItems: "center",
  },
  title: {
    marginBottom: 10,
  },
  settingsCard: {
    width: "90%",
    alignItems: "center",
    marginBottom: 10,
  },
  settingsItem: {
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
  },
  settingsItemHeader: {
    width: "100%",
    alignItems: "center",
    marginBottom: 5,
    backgroundColor: colors.background,
  },
  settingsInputContainer: {
    width: 180,
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "center",
  },
  settingsText: {
    textAlignVertical: "top",
    fontSize: 15,
    marginRight: 5,
    marginBottom: 2,
  },
  input: {
    textAlign: "center",
    borderBottomWidth: 0,
    color: colors.text,
    backgroundColor: colors.background,
    borderRadius: 5,
    alignSelf: "flex-start",
    marginTop: 0,
    height: 25,
    width: 25,
  },
  resultCard: {
    flex: 1,
    marginBottom: 10,
    width: "90%",
  },
  resultTitle: {
    alignItems: "center",
  },
  listContainer: {
    flex: 1,
  },
  flatList: {
    flexGrow: 1,
  },
  listItem: {
    flexDirection: "row",
    marginBottom: 2,
    height: 31.7, // => 33.7
    backgroundColor: colors.accent,
  },
  listIndex: {
    alignSelf: "center",
    padding: 5,
    borderRadius: 15,
    height: 25,
    minWidth: 25,
    alignItems: "center",
    justifyContent: "center",
  },
  listIndexText: {
    fontSize: 12,
    color: colors.card,
  },
  listNumber: {
    padding: 5,
    marginLeft: 5,
    borderRadius: 10,
  },
  listSpecial: {
    padding: 5,
    borderRadius: 10,
    marginLeft: 5,
  },
  listText: {
    fontSize: 16,
  },
  controlButtonArea: {
    width: "100%",
  },
  lottoControlArea: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  controlButton: {
    flex: 1,
    marginLeft: 10,
  },
  navigationContainer: {},
});
