import React, { useState } from "react";
import {
  Dimensions,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
} from "react-native";

import LottoListOverlay from "../screens/LottoListOverlay";

import BodyText from "../components/BodyText";
import ControlButton from "../components/ControlButton";
import Card from "../components/Card";

/**
 * Returns the lottery type selection screen that is initially displayed where
 * the user can choose continue with the selection of a lottery from a list or
 * to create a lottery from custom settings.
 * 
 * @param {*} props onBack={props.onBack}
                    lottery={lottery}
                    lSelectVisible={lSelectVisible}
                    listVisible={listVisible}
                    lSelectVisibleSwitcher={lSelectVisibleSwitcher}
                    listVisibleSwitcher={listVisibleSwitcher}
                    setLotteryHandler={setLotteryHandler}
                    setType={setType}
 */
const LottoSelect = (props) => {
  console.log("LottoSelect");
  console.log(Dimensions.get("window").height);
  let cardContainerResponsive;
  if (Dimensions.get("window").height < 550) {
    cardContainerResponsive = styles.cardContainerSmall;
  }

  return (
    <View style={styles.screen}>
      <LottoListOverlay
        listVisible={props.listVisible}
        listVisibleSwitcher={props.listVisibleSwitcher}
        lSelectVisibleSwitcher={props.lSelectVisibleSwitcher}
        lottery={props.lottery}
        setLotteryHandler={props.setLotteryHandler}
        setType={props.setType}
      />
      <Card style={{ ...styles.cardContainer, ...cardContainerResponsive }}>
        <BodyText style={{ ...styles.instructionText, marginBottom: 20 }}>
          Choose a lottery from a selection or create a custom lottery:
        </BodyText>
        <View style={styles.selectButtonContainer}>
          <View style={styles.selectButton}>
            <ControlButton onPress={() => props.listVisibleSwitcher()}>
              Selection
            </ControlButton>
          </View>
          <View style={styles.selectButton}>
            <ControlButton
              onPress={() => {
                props.lSelectVisibleSwitcher();
                props.setType("C");
              }}
            >
              Custom
            </ControlButton>
          </View>
        </View>
      </Card>

      <TouchableWithoutFeedback>
        <View style={styles.controlButtonArea}>
          <View style={styles.controlButtonContainer}>
            <ControlButton onPress={() => props.onBack("AppSelectionScreen")}>
              Back
            </ControlButton>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};
console.log("LottoSelect end");
export default LottoSelect;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 22,
  },
  cardContainer: {
    marginTop: 30,
    width: "80%",
    alignItems: "center",
    justifyContent: "space-around",
    maxHeight: 350,
    height: Dimensions.get("window").height * 0.5,
  },
  cardContainerSmall: {
    marginTop: 5,
  },
  instructionText: {
    width: "80%",
    textAlign: "center",
  },
  selectButtonContainer: {
    height: Dimensions.get("window").height * 0.3,
    width: "80%",
    justifyContent: "space-around",
    alignItems: "center",
    marginBottom: 10,
  },
  selectButton: {
    width: 200,
  },
  controlButtonArea: {
    height: 50,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "flex-end",
    marginBottom: 10,
  },
  controlButtonContainer: {
    flex: 1,
    marginHorizontal: 10,
  },
});
