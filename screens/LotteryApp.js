import React, { useState } from "react";
import {
  View,
  StyleSheet,
} from "react-native";

import BodyText from "../components/BodyText";

import LottoSelect from "./LottoSelect";
import LottoCustomScreen from "./LottoCustomScreen";
import LottoSelectionScreen from "./LottoSelectionScreen";

import data from "../constants/data";
import colors from "../constants/colors";

/**
 * Returns an HTML element for the special number field of a lotto draw item in
 * a list of lottery draws.
 * 
 * @param {*} itemData 
 */
const renderSpecial = (itemData) => {
  if (Array.isArray(itemData.item.draw[1]) && itemData.item.draw[1].length) {
    const drawSpecials = itemData.item.draw[1].join(", ");
    return (
      <View style={styles.listSpecial}>
        <BodyText style={styles.listText}>{drawSpecials}</BodyText>
      </View>
    );
  } else {
    return null;
  }
};

/**
 * Returns an HTML element for the regular and special number fields of a lotto
 * draw item in a list of lottery draws.
 * 
 * @param {*} itemData the lottery draw item
 */
const renderListItem = (itemData) => {
  // console.log("renderListItem called");
  // console.log(itemData);
  const drawNumbers = itemData.item.draw[0].join(", ");

  return (
    <View style={styles.listItem}>
      <View style={styles.listIndex}>
        <BodyText style={styles.listIndexText}>{itemData.index + 1}</BodyText>
      </View>
      <View style={styles.listNumber}>
        <BodyText style={styles.listText}>{drawNumbers}</BodyText>
      </View>
      {renderSpecial(itemData)}
    </View>
  );
};

/**
 * Returns the lottery app screen.
 * 
 * @param {*} props receives onBack={startAppHandler} to return to selection
 *                  screen.
 */
const LotteryApp = (props) => {
  const [lSelectVisible, setLSelectVisible] = useState(true);
  const [listVisible, setListVisible] = useState(false);
  const [lottery, setLottery] = useState(data.lotterySelection[0]);
  const [type, setType] = useState("");
  const [drawList, setDrawList] = useState([]);
  const [numberAmount, setNumberAmount] = useState("5");
  const [numberField, setNumberField] = useState("50");
  const [specialAmount, setSpecialAmount] = useState("2");
  const [specialField, setSpecialField] = useState("10");

  /**
   * Toggle the visibility of the LottoSelect screen where the user can choose
   * to pick a lottery from a list or use custom settings.
   */
  const lSelectVisibleSwitcher = () => {
    setLSelectVisible(!lSelectVisible);
  };

  /**
   * Toggle the visibility of the LottoListOverlay which renders the lottery
   * picker.
   */
  const listVisibleSwitcher = () => {
    setListVisible(!listVisible);
  };

  /**
   * Sets the name of the lottery as picked from the list. Is passed through
   * LottoSelect and set in LottoListOverlay.
   * 
   * @param {*} item name of the lottery
   */
  const setLotteryHandler = (item) => {
    setLottery(item);
  };

  /**
   * Adds a draw object to the drawList useState.
   * 
   * @param {*} numbers   the list of regular numbers
   * @param {*} specials  the list of special numbers
   */
  const addDrawHandler = (numbers, specials) => {
    // console.log("addDrawHandler called");
    // console.log(numbers + " " + specials);
    const newKey = Date.now() + (0 | (Math.random() * 9e6)).toString(36);
    setDrawList([
      ...drawList,
      { key: newKey, draw: [numbers, specials], lottery: lottery.name },
    ]);
  };

  /**
   * Sets the drawList to an empty array.
   */
  const clearListHandler = () => {
    // console.log("clearListHandler called");
    setDrawList([]);
    // console.log(drawList);
  };

  /**
   * If in custom lottery mode sets the lottery to default settings. Erases
   * the drawList.
   * @param {*} type 
   */
  const clearCustomLottoHandler = (type) => {
    if (type === "C") {
    } else if (type === "S") {
      setNumberAmount("5");
      setNumberField("50");
      setSpecialAmount("2");
      setSpecialField("10");
    }
    clearListHandler();
  };

  /**
   * Generates a draw of regular and special lottery numbers and adds it to the
   * drawList.
   * 
   * @param {*} settings the settings to apply for the lottery draw in an array
   */
  const makeDraw = (settings) => {
    // console.log("makeDraw called");
    if (drawList.length >= 99) {
      // ToDo: Alert
      return null;
    }
    const numbersToDraw = settings[0];
    const numberFieldSize = settings[1];
    const specialToDraw = settings[2];
    const specialFieldSize = settings[3];
    // Create number and special number fields

    // 1. Create numbers field
    let numbersDraw = [];
    let numberField = [];
    let specialDraw = [];
    let specielField = [];

    let count = 1;
    let i;
    for (i = 0; i < numberFieldSize; i++) {
      numberField.push(count);
      count++;
    }
    // 2. Draw numbers
    for (i = 0; i < numbersToDraw; i++) {
      const sizeOfField = numberField.length;
      const index = Math.floor(Math.random() * sizeOfField);
      numbersDraw.push(parseInt(numberField[index], 10));
      numberField.splice(index, 1);
    }
    numbersDraw.sort((a, b) => a - b);

    // 3. Create special number field
    count = 1;
    for (i = 0; i < specialFieldSize; i++) {
      specielField.push(count);
      count++;
    }
    // 4. Draw special numbers
    for (i = 0; i < specialToDraw; i++) {
      const sizeOfField = specielField.length;
      const index = Math.floor(Math.random() * sizeOfField);
      specialDraw.push(parseInt(specielField[index], 10));
      specielField.splice(index, 1);
    }
    specialDraw.sort((a, b) => a - b);
    addDrawHandler(numbersDraw, specialDraw);
  };

  let screen;
  if (lSelectVisible) {
    screen = (
      <LottoSelect
        onBack={props.onBack}
        lottery={lottery}
        lSelectVisible={lSelectVisible}
        listVisible={listVisible}
        lSelectVisibleSwitcher={lSelectVisibleSwitcher}
        listVisibleSwitcher={listVisibleSwitcher}
        setLotteryHandler={setLotteryHandler}
        setType={setType}
      />
    );
  } else if (type === "S") {
    screen = (
      <LottoSelectionScreen
        lSelectVisibleSwitcher={lSelectVisibleSwitcher}
        setType={setType}
        makeDraw={makeDraw}
        lottery={lottery}
        drawList={drawList}
        clearListHandler={clearListHandler}
        renderSpecial={renderSpecial}
        renderListItem={renderListItem}
      />
    );
  } else if (type === "C") {
    screen = (
      <LottoCustomScreen
        lSelectVisibleSwitcher={lSelectVisibleSwitcher}
        setType={setType}
        makeDraw={makeDraw}
        lottery={lottery}
        drawList={drawList}
        clearListHandler={clearListHandler}
        renderSpecial={renderSpecial}
        renderListItem={renderListItem}
        numberAmount={numberAmount}
        setNumberAmount={setNumberAmount}
        numberField={numberField}
        setNumberField={setNumberField}
        specialAmount={specialAmount}
        setSpecialAmount={setSpecialAmount}
        specialField={specialField}
        setSpecialField={setSpecialField}
        clearCustomLottoHandler={clearCustomLottoHandler}
      />
    );
  }

  return <View style={styles.screen}>{screen}</View>;
};

export default LotteryApp;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  buttonContainer: {
    width: 120,
  },

  listItem: {
    flexDirection: "row",
    marginBottom: 2,
    height: 31.7, // => 33.7
    backgroundColor: colors.accent,
  },
  listIndex: {
    alignSelf: "center",
    padding: 5,
    borderRadius: 15,
    height: 25,
    minWidth: 25,
    alignItems: "center",
    justifyContent: "center",
  },
  listIndexText: {
    fontSize: 12,
    color: colors.card,
  },
  listNumber: {
    padding: 5,
    marginLeft: 5,
    borderRadius: 10,
  },
  listSpecial: {
    padding: 5,
    borderRadius: 10,
    marginLeft: 5,
  },
  listText: {
    fontSize: 16,
  },
});
