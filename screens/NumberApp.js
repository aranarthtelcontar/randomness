import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
  FlatList,
} from "react-native";

import BodyText from "../components/BodyText";
import TitleText from "../components/TitleText";
import Card from "../components/Card";
import AppSelectButton from "../components/AppSelectButton";
import ControlButton from "../components/ControlButton";
import Input from "../components/Input";
import colors from "../constants/colors";

/**
 * Returns a number a String with leading zeros.
 * 
 * @param {*} number  a number as String or numerical format.
 * @param {*} width   the number of places including leading zeros.
 */
const zeroFill = (number, width) => {
  // Solution 1:
  // width -= number.toString().length;
  // if (width > 0) {
  //   return (
  //     new Array(width + /\./.test(number) ? 2 : 1).join("0") + number
  //   );
  // } else {
  //   return number + "";
  // }

  // Solution 2:
  let isNegative = false;
  let s = number + "";
  if (number.charAt(0) === "-") {
    isNegative = true;
    s = s.replace("-", "");
  }
  while (s.length < width) s = "0" + s;
  if (isNegative) {
    s = "-" + s;
  }
  return s;
};

/**
 * Returns an HTML element based on a received item.
 * 
 * @param {*} listLength  the lenght of the list.
 * @param {*} itemData    an item of the list.
 */
const renderListItem = (listLength, itemData) => {
  return (
    <View style={styles.listItem}>
      <View style={styles.indexContainer}>
        <BodyText style={styles.listIndex}>
          {listLength - itemData.index}
        </BodyText>
      </View>
      <BodyText style={styles.listText} selectable={true}>
        {zeroFill(itemData.item.value, itemData.item.length)}
      </BodyText>
    </View>
  );
};

/**
 * Returns the number app screen.
 * 
 * @param {*} props receives onBack={startAppHandler} to return to selection
 *                  screen.
 */
const NumberApp = (props) => {
  const [lowerBound, setLowerBound] = useState("1");
  const [upperBound, setUpperBound] = useState("10");
  const [resultsKeyCount, setResultsKeyCount] = useState(1);
  const [results, setResults] = useState([]);
  const maxNumLen = 15;
  const [lowerNumberSign, setLowerNumberSign] = useState("");
  const [upperNumberSign, setUpperNumberSign] = useState("");

  /**
   * Removes a minus sign and sets the lowerNumberSign useState accordingly.
   * Checks the String for maximum length. Sets lowerBound useState.
   * 
   * @param {*} inputText a number as String.
   */
  const minInputHandler = (inputText) => {
    if (inputText.charAt(inputText.length - 1) === "-") {
      if (lowerNumberSign === "") {
        setLowerNumberSign("-");
      } else {
        setLowerNumberSign("");
      }
      inputText.replace("-", "");
    }
    if (inputText.length === maxNumLen + 1) {
      inputText = inputText.slice(0, -1);
    }
    inputText = inputText.replace(/[^0-9]/g, "");
    setLowerBound(inputText);
  };

  /**
   * Removes a minus sign and sets the upperNumberSign useState accordingly.
   * Checks the String for maximum length. Sets upperBound useState.
   * 
   * @param {*} inputText a number as String.
   */
  const maxInputHandler = (inputText) => {
    if (inputText.charAt(inputText.length - 1) === "-") {
      if (upperNumberSign === "") {
        setUpperNumberSign("-");
      } else {
        setUpperNumberSign("");
      }
      inputText.replace("-", "");
    }
    if (inputText.length === maxNumLen + 1) {
      inputText = inputText.slice(0, -1);
    }
    inputText = inputText.replace(/[^0-9]/g, "");
    setUpperBound(inputText);
  };

  /**
   * Resets all number useStates to null.
   */
  const resetInputHandler = () => {
    setLowerBound("");
    setUpperBound("");
    // ToDo: Reset resultsKeyCount and list
    setResults([]);
    setResultsKeyCount(1);
    setLowerNumberSign("");
    setUpperNumberSign("");
  };

  const addResultKey = () => {
    setResultsKeyCount(resultsKeyCount + 1);
  };

  /**
   * Generates a random number between lowerBound and upperBound useStat and
   * Sets results useState with the result.
   */
  const generateRN = () => {
    const min = parseInt(lowerNumberSign + lowerBound, 10);
    const max = parseInt(upperNumberSign + upperBound, 10);
    // console.log("min: " + min.toString().length + ", max: " + max.toString().length)

    // Check if the numbers are entered properly
    if (isNaN(min) || isNaN(max) || max <= min) {
      Alert.alert(
        "Invalid number!",
        "The first number is the lower bound and cannot be bigger than the second number which is the upper bound.",
        [{ text: "Okay", style: "destructive", onPress: resetInputHandler }]
      );
      return;
    }

    // Get the max number places
    let numLength;
    const minLen = min.toString().replace("-", "").length;
    const maxLen = max.toString().replace("-", "").length;
    if (minLen < maxLen) {
      numLength = maxLen;
    } else {
      numLength = minLen;
    }

    let randomNumber =
      Math.floor(Math.random() * Math.floor(max + 1 - min)) + Math.floor(min);
    let newKey = resultsKeyCount + Math.random();
    addResultKey();
    setResults((curResults) => [
      {
        key: newKey.toString(),
        value: randomNumber.toString(),
        length: numLength,
      },
      ...curResults,
    ]);
  };

  return (
    <View style={styles.screen}>
      <View style={styles.titleContainer}>
        <TitleText style={styles.title}>Random Number</TitleText>
      </View>
      <Card style={styles.cardContainer}>
        <View style={styles.cardItem}>
          <BodyText style={styles.cardText}>From</BodyText>
          <Card style={styles.inputCard}>
            <BodyText style={styles.inputNumberSign}>
              {lowerNumberSign}
            </BodyText>
            <Input
              style={styles.input}
              blurOnSubmit
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="number-pad"
              maxLength={maxNumLen + 1}
              onChangeText={minInputHandler}
              value={lowerBound}
            ></Input>
          </Card>
        </View>
        <View style={styles.cardItem}>
          <BodyText style={styles.cardText}>To</BodyText>
          <Card style={styles.inputCard}>
            <BodyText style={styles.inputNumberSign}>
              {upperNumberSign}
            </BodyText>
            <Input
              style={styles.input}
              blurOnSubmit
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="number-pad"
              maxLength={maxNumLen + 1}
              onChangeText={maxInputHandler}
              value={upperBound}
            ></Input>
          </Card>
        </View>
      </Card>

      <Card style={styles.listCardContainer}>
        <View style={styles.listContainer}>
          <FlatList
            data={results}
            renderItem={renderListItem.bind(this, results.length)}
            contentContainerStyle={styles.list}
          />
        </View>
      </Card>

      {/*         <FlatList
              data={results}
              renderItem={renderListItem.bind(this, results.length)}
              // renderItem={({ item }) => <BodyText>{item.value}</BodyText>}
              // renderItem={({ item }) =>
              //   <renderListItem(results.length, item.value)
              // }
              contentContainerStyle={styles.list}
            /> */}

      {/* <View style={styles.cardItem}>
            <Card style={styles.outputCard}>
              <FlatList></FlatList>
            </Card>
          </View> */}
      <TouchableWithoutFeedback>
        <View style={styles.buttonContainer}>
          <AppSelectButton onPress={() => props.onBack("AppSelectionScreen")}>
            Back
          </AppSelectButton>
          <ControlButton onPress={generateRN}>Generate</ControlButton>
          <ControlButton onPress={resetInputHandler}>Clear</ControlButton>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default NumberApp;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },
  marginOnTop: {
    marginTop: 15,
  },

  titleContainer: {
    width: "80%",
  },
  title: {
    fontSize: 20,
    marginBottom: 10,
    fontFamily: "open-sans-bold",
    textAlign: "center",
  },

  cardContainer: {
    marginBottom: 15,
    paddingTop: 0,
    justifyContent: "center",
    alignItems: "flex-start",
    width: "90%",
  },
  cardItem: {
    flexDirection: "row",
    width: "90%",
    alignItems: "center",
    marginTop: 20,
  },
  cardText: {
    fontSize: 17,
    width: 45,
    textAlign: "right",
    marginRight: 20,
  },
  inputCard: {
    flexDirection: "row",
    width: "85%",
    backgroundColor: colors.background,
  },
  inputNumberSign: {
    width: 10,
    fontSize: 20,
  },
  input: {
    textAlign: "center",
    fontSize: 20,
    color: colors.text,
    minWidth: "85%",
    maxWidth: "95%",
  },
  listCardContainer: {
    flex: 1,
    width: "90%",
  },
  listContainer: {
    flex: 1,
  },
  list: {
    width: "100%",
  },
  indexContainer: {
    justifyContent: "center",
    marginRight: 10,
    width: 25,
    borderRadius: 10,
    paddingLeft: 3,
  },
  listIndex: {
    textAlign: "center",
    fontSize: 12,
    color: colors.card,
  },
  listText: {
    fontSize: 20,
  },
  listItem: {
    borderColor: "#ccc",
    borderWidth: 1,
    paddingVertical: 2,
    paddingLeft: 2,
    paddingRight: 10,
    flexDirection: "row",
    backgroundColor: colors.background,
  },

  buttonContainer: {
    height: 50,
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    marginTop: 15,
    alignItems: "flex-end",
  },
  button: {
    width: "80%",
    marginVertical: 10,
  },
});
