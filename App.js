import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";

import Header from "./components/Header";
import colors from "./constants/colors";
import AppSelectionScreen from "./screens/AppSelectionScreen";
import NumberApp from "./screens/NumberApp";
import DiceApp from "./screens/DiceApp";
import LotteryApp from "./screens/LotteryApp";

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });
};

/**
 * Main function. Returns a screen according to app selection. Initially will
 * return the app selection screen.
 */
export default function App() {
  console.log("App is executed ###########################################");
  const [selectedApp, setSelectedApp] = useState("AppSelectionScreen");
  // Halt the progression of the programm to load fonts.
  // Therefore create a state that keeps track of it.
  const [dataLoaded, setDataLoaded] = useState(false);
  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
        onError={(err) => console.log(err)}
      />
    );
  }

  const startAppHandler = (appSelection) => {
    setSelectedApp(appSelection);
  };

  let content;
  if (selectedApp === "NumberApp") {
    content = <NumberApp onBack={startAppHandler} />;
  } else if (selectedApp === "DiceApp") {
    content = <DiceApp onBack={startAppHandler} />;
  } else if (selectedApp === "LotteryApp") {
    content = <LotteryApp onBack={startAppHandler} />;
  } else if (selectedApp === "AppSelectionScreen") {
    content = <AppSelectionScreen onStartApp={startAppHandler} />;
  }
  return (
    <View style={styles.screen}>
      <Header title="Randomness" />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colors.background,
  },
});
