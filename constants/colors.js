export default {
  // primary: "#e55d42",
  // accent: "#e5429e",
  primary: "#625750",
  accent: "#96897f",
  text: "white",
  textDark: "black",
  background: "#96897f",
  card: "#c6bcb6"
};

// https://www.w3schools.com/colors/colors_palettes.asp