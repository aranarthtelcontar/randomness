export default {
  diceSides: ["4", "6", "8", "10", "12", "20", "30", "50", "100"],
  diceModifiers: [
    "-5",
    "-4",
    "-3",
    "-2",
    "-1",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
  ],

  lotterySelection: [
    { key: "01", name: "Powerball USA", fields: "5/69 + 1/26", settings: [5, 69, 1, 26] },
    { key: "02", name: "Mega Millions USA", fields: "5/75 + 1/15", settings: [5, 75, 1, 15] },
    { key: "03", name: "Eurojackpot", fields: "5/50 + 2/10", settings: [5, 50, 2, 10] },
    { key: "04", name: "EuroMillions", fields: "5/50 + 2/12", settings: [5, 50, 2, 12] },
    { key: "05", name: "Lotto Austria", fields: "6/45", settings: [6, 45, -1, -1] },
    { key: "06", name: "Danske Lotto Denmark", fields: "5/50 + 2/12", settings: [5, 50, 2, 12] },
    { key: "07", name: "Finnland", fields: "7/40", settings: [7, 40, -1, -1] },
    { key: "08", name: "France", fields: "5/49 + 1/10", settings: [5, 49, 1, 10] },
    { key: "09", name: "Lotto Germany", fields: "6/49 + 1/10", settings: [6, 49, 1, 10] },
    { key: "10", name: "Hungary", fields: "5/90", settings: [5, 90, -1, -1] },
    { key: "11", name: "Lotto 6/49", fields: "6/49", settings: [6, 49, -1, -1] },
    { key: "12", name: "Italy", fields: "6/90", settings: [6, 90, -1, -1] },
    { key: "23", name: "Lituania", fields: "6/30", settings: [6, 30, -1, -1] },
    { key: "13", name: "Netherlands", fields: "6/45 + 1/6", settings: [6, 45, 1, 6] },
    { key: "14", name: "Portugal", fields: "5/49 + 1/13", settings: [5, 49, 1, 13] },
    { key: "15", name: "Sweden", fields: "7/35", settings: [7, 35, -1, -1] },
    { key: "16", name: "Switzerland", fields: "6/42 + 1/6", settings: [6, 42, 1, 6] },
    { key: "17", name: "UK", fields: "6/59", settings: [6, 59, -1, -1] },

  ],
};

// https://de.wikipedia.org/wiki/Lotto
// https://en.wikipedia.org/wiki/Lotteries_by_country
// https://en.wikipedia.org/wiki/List_of_lotteries