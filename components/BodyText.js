import React from "react";
import { Text, StyleSheet } from "react-native";

import colors from "../constants/colors";

const BodyText = props => <Text style={{...styles.body, ...props.style}}>{props.children}</Text>;

const styles = StyleSheet.create({
  body: {
    fontFamily: "open-sans",
    color: colors.text
  }
});

export default BodyText;