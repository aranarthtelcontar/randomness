import React from "react";
import { View, StyleSheet } from "react-native";

import TitleText from "../components/TitleText";
import colors from "../constants/colors";

const Header = props => {
  return (
    <View style={styles.header}>
      <TitleText style={styles.titleText}>{props.title}</TitleText>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 80,
    paddingTop: 10,
    backgroundColor: colors.primary,
    alignItems: "center",
    justifyContent: "center"
  },
  titleText: {
    color: "white",
    fontSize: 23
  }
});

export default Header;