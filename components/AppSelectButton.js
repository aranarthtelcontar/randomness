import React from "react";
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";

import colors from "../constants/colors";

const AppSelectButton = props => {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
      <View style={{...styles.button, ...props.style}}>
        <Text style={styles.buttonText}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 10
  },
  buttonText: {
    color: colors.text,
    fontFamily: "open-sans",
    fontSize: 18,
    textAlign: "center"
  }
});

export default AppSelectButton;