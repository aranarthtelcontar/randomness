import React, { useState } from "react";
import { View, Picker, StyleSheet } from "react-native";

const PickerComponent = (props) => {
  console.log("PickerComponent called");
  // props.pickerItems.forEach((element) => {
  //   pickerItems.push(<Picker.Item label={element} value={element} />);
  // });

  const update = (newValue) => {
    console.log("PickerComponent > update called");
    props.updateDice(props.dice.key, newValue);
  };
  return (
    <View style={styles.container}>
      <Picker
        selectedValue={props.selectedValue}
        style={{ height: 50, width: 200 }}
        onValueChange={update}
      >
        {/* {props.pickerItems.map((item) => {
          return <Picker.Item label={item} value={item} />;
        })} */}
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: "center",
  },
});

export default PickerComponent;

<Picker
  selectedValue={this.state.currency}
  onValueChange={(itemValue, itemIndex) => this.pickerChange(itemIndex)}
>
  {this.state.currencies.map((v) => {
    return <Picker.Item label={v.currencyLabel} value={v.currency} />;
  })}
</Picker>;
