import React from "react";
import { StyleSheet } from "react-native";

import BodyText from "../components/BodyText";

const TitleText = props => (
  <BodyText style={{ ...styles.title, ...props.style }}>{props.children}</BodyText>
);

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontFamily: "open-sans-bold"
  }
});

export default TitleText;
