import React from "react";
import { View, StyleSheet } from "react-native";

import colors from "../constants/colors";

const Card = props => {
  return (
    <View style={{ ...styles.card, ...props.style }}>{props.children}</View>
  );
};

const styles = StyleSheet.create({
  card: {
    // Android
    elevation: 5,
    // iOS
    shadowColor: "black",
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 0.26,

    backgroundColor: colors.card,
    padding: 20,
    borderRadius: 10
  }
});

export default Card;
