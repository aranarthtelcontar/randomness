import React from "react";
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";

import colors from "../constants/colors";

const RemoveItemButton = props => {
  return (
    <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
      <View style={styles.button}>
        <Text style={styles.buttonText}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    // paddingVertical: 12,
    // paddingHorizontal: 15,
    padding: 5,
    alignItems: "center",
  },
  buttonText: {
    color: colors.text,
    fontFamily: "open-sans",
    fontSize: 15,
    textAlign: "center"
  }
});

export default RemoveItemButton;